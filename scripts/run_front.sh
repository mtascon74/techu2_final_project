#!/bin/bash
# Builds a new docker for front image

# the cd trick assures this works even if the current directory is not current.
cd ../front

sudo docker stop fronttechu
sudo docker rm fronttechu
sudo docker run -p 4000:4000 --name fronttechu --net techunet -d mtascon/mifront
# Do the build
