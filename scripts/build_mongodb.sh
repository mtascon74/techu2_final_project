#!/bin/bash
# Builds a new docker for MongoDB

# the cd trick assures this works even if the current directory is not current.
cd ../MongodbDocker

sudo docker build -t mtascon/localmongodb .
# Do the build
