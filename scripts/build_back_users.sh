#!/bin/bash
# Builds a new docker for front image

# the cd trick assures this works even if the current directory is not current.
cd ../back-users

npm build
sudo docker build -t mtascon/miback .
# Do the build
