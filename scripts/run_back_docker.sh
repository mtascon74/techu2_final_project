#!/bin/bash
# Builds a new docker for front image

# the cd trick assures this works even if the current directory is not current.
cd ../back

sudo docker stop techubankserver
sudo docker rm techubankserver
sudo docker run --add-host=files.labstss.com:127.0.0.1 -p 4430:4430 --net techunet --name techubankserver mtascon/techubankback
# Do the build
