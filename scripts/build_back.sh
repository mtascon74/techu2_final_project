#!/bin/bash
# Builds a new docker for front image

# the cd trick assures this works even if the current directory is not current.
cd ../back

npm build
sudo docker build -t mtascon/techubankback .
# Do the build
