#!/bin/bash
# Run a new docker for Local MongoDB as

# the cd trick assures this works even if the current directory is not current.
cd ../MongodbDocker

sudo docker rm mongodbserver
sudo docker run -v /opt/datosmongo:/data/db -p 27017:27017 --net techunet --name mongodbserver mtascon/localmongodb

