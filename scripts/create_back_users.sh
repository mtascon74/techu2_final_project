#!/bin/bash
# Builds a new docker for front image

# the cd trick assures this works even if the current directory is not current.
cd ../back_users

npm init
npm install nodemon --save
npm install express --save
npm install fs --save
npm install https --save
npm install cors --save
npm install pg --save
npm install request-json --save
npm install dotenv --save

