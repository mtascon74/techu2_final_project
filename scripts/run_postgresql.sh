#!/bin/bash
# Builds a new docker for PostgreSQL	

# the cd trick assures this works even if the current directory is not current.
cd ../PostgresqlDocker

sudo docker rm postgresqlserver

sudo docker run -p 5433:5432 --volume pgdata:/var/lib/postgresql/9.3/main --net techunet --name postgresqlserver mtascon/localpostgresql 

