#!/bin/bash
# Builds a new docker for front image

# the cd trick assures this works even if the current directory is not current.
cd ../front

polymer build
sudo docker build -t mtascon/mifront .
# Do the build
