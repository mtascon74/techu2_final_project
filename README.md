![Alt text](./doc/images/logo-tech-u.svg?raw=true "Title")


BBVA Tech University. Second edition. Final Project

Author: Manuel Tascón Alvarez

La carpeta front contiene la parte frontal web del proyecto realizada con tecnologías Polymer y Cells.

La carpeta back contiene la parte back del proyecto realizada con tecnologías Node.js.

Las carpetas MongodbDocker y PostgresqlDocker contienen sendos Dockerfile para generar los contenedores de las Bases de Datos del proyecto. Postgresql se utiliza para la base de datos de clientes y MongoDB como base de datos de cuentas y transacciones.

La carpeta scripts contiene scripts con comandos habitualmente utilizados en el proyecto como la creación de los contenedores y el arranque de los mismos.

La carpeta doc contiene la Guía de usuario de la aplicación
