
BBVA Tech University. Second edition. Final Project

Author: Manuel Tascón Alvarez

Basado en el código base y estructura de Cells Static Sample App
Páginas y  Web Components para el proyecto final de Tech University. Bootcamp Practitioner. 2nd Edition

Del README.md original:

# Cells Static Sample App

Sample app showcasing the use and features of Cells Static library in combination with Polymer CLI tooling to build modular, maintainable, & high scalable Progressive Web Apps based on Polymer 2
