// TechU Bank Final Project. Front Contents 
// Comments: Manuel Tascon Alvarez. Based on static-cells-sample-app repository code. 
// Changes in routes and new logoutChannel


(function(window) {
  var ROUTER_HOOKS = getRouterHooks();
  var ROUTES = getRoutes();
  var DEFAULT_ROUTE = ROUTES[0].name;

  Cells.config({
    pageRender: {
      mainNode: '#app__container',
      componentPath: 'app/pages',
      progressive: true,
      maxSimultaneousPages: 9,
    },
    router: {
      useHash: true,
      routes: ROUTES,
      defaultRoute: DEFAULT_ROUTE,
      hooks: ROUTER_HOOKS,
    },
  });

  function getRouterHooks() {
    var transitionChannel = Cells.Bus.channel('transition');
    var pageTitleChannel = Cells.Bus.channel('page_title');
    var errorChannel = Cells.Bus.channel('error');
    var logoutChannel = Cells.Bus.channel('logout');

    return {
      onTransitionStart: function(toState, fromState) {
        transitionChannel.publish('onTransitionStart');
        pageTitleChannel.publish('Starting Transitioning to...' + toState.name);
      },
      onTransitionActivate: function(toState, fromState) {
        transitionChannel.publish('onTransitionActivate');
        pageTitleChannel.publish('Activating transition to...' + toState.name);
        if(toState.name=='logout')
        {
            logoutChannel.publish('logout');
        }

        return Promise.resolve({});
      },
      onTransitionResolve: function(toState, fromState) {
        transitionChannel.publish('onTransitionResolve');
        pageTitleChannel.publish('Resolving transition to...' + toState.name);

        return Promise.resolve({});
      },
      onTransitionCancel: function(toState, fromState) {
        transitionChannel.publish('onTransitionCancel');
      },
      onTransitionError: function(toState, fromState, err) {
        transitionChannel.publish('onTransitionError');

        if (err && err.description) {
          errorChannel.publish(err.description);
        }
      },
      onTransitionSuccess: function(toState, fromState) {
        transitionChannel.publish('onTransitionSuccess');
        pageTitleChannel.publish(toState.name);
      },
    };
  }

  function getRoutes() {
    var ROUTE_HOOKS = getRouteHooks();
    
    return [
      createRoute('login', '/login', {}, {}),
      createRoute('loginerror', '/loginerror', {}, {}),
      createRoute('logout', '/logout', {},{}),
      createRoute('register', '/register', {}, {}),
      createRoute('recover-password', '/recover-password', {}, {}),
      createRoute('products', '/products', {private: true}, ROUTE_HOOKS),
      createRoute('product-transactions', '/product-transactions', {private: true}, ROUTE_HOOKS),
      createRoute('restricted-access', '/forbidden', {}, {})
    ];
  }

  function getRouteHooks() {
    return {
      onActivate: function(toState, fromState, done) {
        var route = ROUTES.find((route) => route.name === toState.name);
        var isPrivate = isPrivateRoute(route);
        var userLogged = isUserLogged();
        
        return new Promise((resolve, reject) => {
          (!isPrivate || isPrivate && userLogged) ? resolve() : reject({description: 'You are trying to access a private page.', redirect: {name: 'restricted-access'}});
        });
      },
      onResolve: function(toState, fromState, done) {
        return Promise.resolve({timestamp: new Date().toString()});
      },
    };
  }

  function createRoute(name, path, data, hooks) {
    return Object.assign({name, path, data}, hooks);
  }

  function isPrivateRoute(state) {
    return state && state.data && state.data.private || false;
  }

  function isUserLogged() {
    var isLoggedValue = window.sessionStorage.getItem('isLogged') === 'true' || false;

    return isLoggedValue;
  }
}(window));
