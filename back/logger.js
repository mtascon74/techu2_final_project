// HTTPS Back Server for Final Project Tech University
// logger. Use winston to log everything
// Author: Manuel Tascon Alvarez

// config file
var config = require('./sec_config');

// wiston for logging
const winston = require('winston');

// logger definition. Default routes ../logs
var logger = new winston.Logger({
  transports: [
      new winston.transports.File({
          level: 'info',
          filename: config.LOGFILE,
          handleExceptions: true,
          json: false,
          maxsize: 5242880, //5MB
          maxFiles: 5,
          colorize: false
      }),
      
      new winston.transports.Console({
          level: 'debug',
          handleExceptions: true,
          json: false,
          colorize: true
      })
  ],
  exitOnError: false
});

logger.stream = {
  write: function(message, encoding){
      logger.info(message);
  }
};

module.exports = logger;
