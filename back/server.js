'use strict';

// HTTPS Back Server for Final Project Tech University
// Author: Manuel Tascon ALvarez

// dotenv Lib for environment variables
require('dotenv').config();

// config to read configuration variables
var config = require('./sec_config');


// logger definition. Default routes ../logs
var logger = require('./logger');
// express Web server Definiton
var express = require('express'),
app = express(),
port = process.env.PORT || 3000,
mongoose = require('mongoose'),
Accounts = require('./api/models/accountsModel'), //created model loading here
Transactions = require('./api/models/transactionsModel'), //created model loading here
morgan = require('morgan'),
bodyParser = require('body-parser');

// fs for filesystem IO reading
var fs = require('fs');

// HTTPS Primary Certificate, Intermediate Certificate & Private Key
var key = fs.readFileSync(config.PRIVATE_KEY);
var cert = fs.readFileSync(config.PRIMARY_CERTIFICATE);
var ca = fs.readFileSync(config.INTERMEDIATE_CERTIFICATE);

var options = {
  key: key,
  cert: cert,
  ca: ca
};

// app express variable
var app = express();


// cors to avoid cross-domain problems. Only for demo purposes
var cors = require('cors');
app.use(cors());

// bcrypt to encrypt user passwords
var bcrypt = require('bcrypt');

// request-json to parse HTTP requests
var requestjson = require('request-json');

// body-parser to parse req.body and avoid undefined
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json()); // for parsing application/json

// Mongoose Initialization
mongoose.connect(config.MONGO_DBURI, {
});

mongoose.Promise = global.Promise;
//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// jwt to handle JWT Tokens
var jwtService = require('./jwtService');

// Importing and registering Routes for accounts & transactions
var acc_routes = require('./api/routes/accountsRoutes'); //importing route
var tran_routes = require('./api/routes/transactionsRoutes');
var cust_routes = require('./api/routes/customersRoutes');
acc_routes(app); //register the route
tran_routes(app);
cust_routes(app);

// https to support https connections
var https = require('https');

// Creating HTTPS Server. Default port 
https.createServer(options, app).listen(config.SECURE_PORT);
logger.info('TechU Project BackEnd HTTPS RESTful API server. Back Server for Final Project Tech University');

// App GET Function with JWT just for test purposes.
app.get('/private', jwtService.checkJWT, function(req, res){
  var token = req.headers.authorization.split(' ')[1];
  res.json({ message: 'You are authenticated and your _id is:'+req.user });
});

