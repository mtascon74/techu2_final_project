// HTTPS Back Server for Final Project Tech University
// jwtService. Use jwt-simple to create and check JSON Web Tokens
// Author: Manuel Tascon Alvarez

var jwt = require('jwt-simple');  
var moment = require('moment');  
var config = require('./sec_config');
var logger = require('./logger');

exports.createToken = function(user) {  
  var payload = {
    sub: user._id,
    iat: moment().unix(),
    exp: moment().add(5, "days").unix(),
  };
  return jwt.encode(payload, config.TOKEN_SECRET);
};

exports.checkJWT = function(req, res, next) { 
    logger.info("jwtService checkJWT");
    const checkedHeader=JSON.stringify(req.headers);
    logger.debug("Header:"+checkedHeader);
    logger.info("Header.customerid:"+req.headers.customerid);
    logger.debug("Header.Authorization: "+req.headers.authorization);      
    if(!req.headers.authorization) {
      return res
        .status(403)
        .send({message: "Error. Unauthorized user and/or token"});
    }
   
    var token = req.headers.authorization.split(" ")[1];
    var payload = jwt.decode(token, config.TOKEN_SECRET);
    logger.debug("Payload:"+JSON.stringify(payload));
    if(payload.sub!=req.headers.customerid)
    {
      return res.status(403).send({message: "Error. Unauthorized user and/or token"});
      

    }
    if(payload.exp <= moment().unix()) {
    return res
      .status(401)
      .send({message: "The token expires"});
    }
   
    req.user = payload.sub;
    next();
};
