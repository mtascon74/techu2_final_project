// HTTPS Back Server for Final Project Tech University
// accountsRoutes
// Author: Manuel Tascon Alvarez
// EndPoints
// GET /all_accounts Get All accounts. Disabled. Only for test purposes
// GET /accounts/:accId/transactions Get Account Info for account with acc_id=accountId
// GET /accounts Get some user's specfic accounts. customerid is sent in header with JWT Token

'use strict';


module.exports = function(app) {
  var accountsList = require('../controllers/accountsController');
  var jwtService = require('../../jwtService');
  
  app.route('/all_accounts')
    .get(accountsList.list_all_accounts);

  app.route('/accounts/:accountId')
    .get(jwtService.checkJWT, accountsList.read_an_specific_account);
  
  app.route('/accounts')
  .get(jwtService.checkJWT, accountsList.read_users_specific_account); 

};
