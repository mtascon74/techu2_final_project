// HTTPS Back Server for Final Project Tech University
// transactionsRoutes
// Author: Manuel Tascon Alvarez
// EndPoints
// GET /accounts/:accId/transactions Get Transactions for an account with acc_id=accId
// GET /transactions Get All transactions. Disabled. Only for test purposes

'use strict';
module.exports = function(app) {
  var transactionsList = require('../controllers/transactionsController');
  var jwtService = require('../../jwtService');
  
//  app.route('/transactions')
//    .get(jwtService.checkJWT, transactionsList.list_all_transactions);
 
  app.route('/accounts/:accId/transactions')
    .get(jwtService.checkJWT, transactionsList.read_account_specific_transactions);       

};