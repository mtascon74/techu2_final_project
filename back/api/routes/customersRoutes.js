// HTTPS Back Server for Final Project Tech University
// customersRoutes
// Author: Manuel Tascon Alvarez
// EndPoints
// POST /customer-password/validations for login operations
// POST /customers for new customers registrations
// HEAD /customers for checking customer existance in new customers registrations
// GET /customers by ID for Customer Information retrival
// POST /customer-password/resets to send email with reseted password.

'use strict';
module.exports = function(app) {
  var customers = require('../controllers/customersController');
  var jwtService = require('../../jwtService');

  // POST customer-password/validations for login operations
  app.route('/customer-password/validations')
    .post(customers.login);

  // POST customers for new customers registrations
  app.route('/customers')
    .post(customers.register_user);

  // HEAD customers for checking customer existance in new customers registrations
  app.route('/customers/:userId')
    .head(customers.checkUserId);
  
  // GET customers by ID for Customer Information retrival
  app.route('/customers/:userId')
    .get(jwtService.checkJWT, customers.getUserData);

  // POST customer-password/resets to send email with reseted password.
  app.route('/customers-passwords/resets')
    .post(customers.resetUserPassword);

};