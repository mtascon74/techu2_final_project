// HTTPS Back Server for Final Project Tech University
// accountsController.
// Author: Manuel Tascon ALvarez

'use strict';

var logger = require('../../logger');

var mongoose = require('mongoose'),
  Accounts = mongoose.model('Accounts');

exports.list_all_accounts = function(req, res) {
  logger.info("Controller: List_all_accounts");
  Accounts.find({}, function(err, account) {
    if (err)
    {
      logger.info("Error in List_all_accounts");

      res.send(err);
    }
    logger.info("Something found");
    logger.debug("cuenta: "+account);
    logger.debug("Res: "+res);
    res.json(account);
    
  });
};

exports.read_an_specific_account = function(req, res) {
  logger.info("Controller: Read and specific account");
  logger.info("Account Id:"+req.params.accountId);
  Accounts.find({'acc_id':req.params.accountId}, function(err, account) {
    if (err)
      res.send(err);
    res.json(account);
  });
};

exports.read_users_specific_account = function(req, res) {
  logger.info("Controller: Read some user's specific accounts");
  logger.info("Headers customerId:"+req.headers.customerid);
  Accounts.find({'customer_id':req.headers.customerid}, function(err, account) {
    if (err)
    {
      logger.debug("Read user's specific accounts error: "+err);
      res.send(err);
    }
    logger.debug("Found accounts: "+JSON.stringify(account));
    res.json(account);
  });
};