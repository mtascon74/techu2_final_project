// HTTPS Back Server for Final Project Tech University
// customersController.
// Author: Manuel Tascon ALvarez


// config to read configuration variables
var config = require('../../sec_config');

// loggger for local Back Server logging
var logger = require('../../logger');

// pg for Postgresql connections
var pg = require('pg');
var postgresClient = new pg.Client(config.POSTGRESQL_DBURI);
postgresClient.connect();


// request for easy 3rd party HTTP Requests
var request = require('request');

// bcrypt to encrypt user passwords
var bcrypt = require('bcrypt');

// jwt to handle JWT Tokens
var jwtService = require('../../jwtService');

// generate-password to resetUserPassword
var generator = require('generate-password');

// POST customer-password/validations for login operations
exports.login = function (req, res) {
  logger.info("Call POST /customer-password/validations");
  logger.info("Parameters. UserID "+req.body.originUserID);
  logger.info("Parameters. UserPassword "+req.body.passwordValue);
  logger.info("Parameters. HashUserPassword "+req.body.passwordValue);
  logger.info("Connecting Local PostgreSQL DB");
  // SQL Query
  const query = postgresClient.query('SELECT * FROM customers_table WHERE username=$1;',[req.body.userValue],(err, result) => {
    var found_result=JSON.stringify(result);
    logger.debug("Query Result: "+found_result);
    logger.debug("Count: "+result.rowCount);
     
    if(err) {
      logger.info("password/validations: Error in SQL SELECT query:");
      logger.debug(err);
      res.send(err);
    }
    else if(result.rowCount == 1) {
      logger.debug("Row[0]: "+JSON.stringify(result.rows[0]));  
      logger.debug("Userpassword: "+result.rows[0].userpassword);
      logger.debug("CustomerID: "+result.rows[0].customerid);
      logger.debug("Username: "+result.rows[0].username);
      logger.debug("Name: "+result.rows[0].name);
      comparePassword(req.body.passwordValue, result.rows[0].userpassword);
      if (!comparePassword(req.body.passwordValue,result.rows[0].userpassword)) {
        logger.info("Incorrect Password. Stored password & typed password do not match");
        res.json({ name : '', customerid : '', token : '', message: 'Fallo Autenticación. Usuario o clave incorrecta.', returnCode : '1'});
      } else {
        logger.info("Correct Password");
        logger.info("Username: "+result.rows[0].username);
        res.json({name : result.rows[0].name, 
          customerid : result.rows[0].customerid, 
          token: jwtService.createToken({_id: result.rows[0].customerid}),
          message: 'Usuario y clave correcta.', returnCode : '0'});
      }
    }
    else
    {
      logger.debug("FOUND: "+result.rowCount);
      logger.info("Incorrect User or Password. For example: duplicaded user");
      res.json({ name : '', customerid : '', token : '', message: 'Fallo Autenticación. Usuario o clave incorrecta.', returnCode : '1'});
    }
  });  
};
    
// POST /customers for new customers registrations
exports.register_user = function (req, res) {
  logger.info("Call POST /customers");
  logger.info("Parameters. UserID "+req.body.userID);
  logger.info("Parameters. UserName "+req.body.userName);
  logger.info("Parameters. LastName "+req.body.userLastName);
  logger.info("Parameters. UserPassword "+req.body.userPasswordValue);
  var hash_password = bcrypt.hashSync(req.body.userPasswordValue, 10);
  logger.debug("Parameters: hash_password:"+hash_password);
  logger.info("Connecting Local PostgreSQL DB");
    
  const query = postgresClient.query('INSERT INTO customers_table(username, name, lastname, userpassword) VALUES($1, $2, $3, $4);',[req.body.userID, req.body.userName, req.body.userLastName, hash_password],(err, result) => {
    if(err) {
      logger.info("POST Customers Failed");
      logger.debug(err);
      res.send(err);
    }
    else {
      logger.info("POST Customers OK");
      logger.debug(result);
      res.send(result);
    }
  });
};
    
    
// HEAD /customers for checking customer existance in new customers registrations
exports.checkUserId =  function (req, res) {
  logger.info("Call HEAD /customers/ with customer userID:"+req.params.userId);
  logger.info("Connecting Local PostgreSQL DB");
  res.setHeader('Content-Type', 'application/json');
      
  const query = postgresClient.query('SELECT customerid FROM customers_table WHERE username=$1;',[req.params.userId],(err, result) => {
    if(err) {
      logger.info("HEAD Customers Failed");
      logger.debug(err);
      res.send(err);
    }
    else {
      logger.info("HEAD Customers OK");
      logger.debug("Result: "+result);
      logger.debug("ROW COUNT: "+result.rowCount);
      if(result.rowCount>=1)
      {
        logger.info("User exists. Return 200 and something");
        res.status(200).send();
      }
      else
      {
        logger.info("User does not exist. Return 404");
        res.status(404).send();
      }
    }
  });  
};
    
// GET customers by ID for Customer Information retrival
exports.getUserData = function (req, res) {
  logger.info("Call GET /customers/ with customer userID:"+req.params.userId);
  logger.info("Connecting Local PostgreSQL DB");
  // SQL SELECT QUERY: SELECT FROM customers_table WHERE userID=req.params.userId;
  const query = postgresClient.query('SELECT * FROM customers_table WHERE customerid=$1;',[req.params.userId],(err, result) => {
        
    if(err) {
      logger.info("GET Customers Failed");
      logger.debug(err);
      res.send(err);
    }
    else {
      logger.info("GET Customers OK");
      logger.debug(result);
      res.send(result);
    }
        // logger.info("Closing PG CLient");
        // postgresClient.end();
  });  
};
    
// POST customer-password/resets to send email with reseted password.
exports.resetUserPassword = function (req, res) {
  logger.info("Call POST /customer-password/resets");
  logger.info("Parameters. Recovery Mail "+req.body.mail);
  var password = generator.generate({
    length: 12,
    numbers: true
  });
  var hash_password = bcrypt.hashSync(password, 10);
  logger.debug("Parameters: hash_password:"+hash_password);
  console.log(password);
  // UPDATE customers_table SET customerid=19 WHERE customerid=21
  const query = postgresClient.query('UPDATE customers_table SET userpassword=$1 WHERE username=$2;'
  ,[hash_password, req.body.mail],(err, result) => {
        
    if(err) {
      logger.info("UPDATE Customers Failed");
      logger.debug(err);
      // res.send(err);
    }
    else {
      logger.info("UPDATE Customers OK");
      logger.debug(result);
      // res.send(result);
    }   
  });     
  var url = config.MAILGUN_URI+'&to='+req.body.mail+'&text=Su nueva clave es ' + password + '. Gracias por utilizar TechUBank. &subject=TechuBank.Password Reset';
  var username = "api";
  var password = config.MAILGUN_APIKEY;
  var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
  // do the POST request
  request.post({
    url: url,
    headers: {
      "Authorization": auth
   }
  }, function (error, response, body) {
    if(error)
    { 
      logger.info("Error while communication with api and ERROR is :  " + error);
      res.send(error);
    }
    logger.debug('body : ', body);
    res.send(body);      
       
  });    
        // postgresClient.connect();
};
      
// Internal comparePassword function for user authentication
function comparePassword(password, hash_password)
{
  logger.info("Calling comparePassword function");
  logger.debug("comparePassword Function. Introducido:"+password+" Hash:"+hash_password);
  return bcrypt.compareSync(password,hash_password);
}
    
