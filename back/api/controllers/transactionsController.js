// HTTPS Back Server for Final Project Tech University
// transactionsController.
// Author: Manuel Tascon ALvarez

'use strict';

var logger = require('../../logger');

var mongoose = require('mongoose'),
Accounts = mongoose.model('Accounts'),
Transactions = mongoose.model('Transactions');

exports.list_all_transactions = function(req, res) {
  logger.info("Controller: List_all_transactions");
  Transactions.find({}, function(err, transactions) {
    if (err)
    {
      logger.info("Error in List_all_transactions");
      res.send(err);
    }
    logger.info("Something found")
    res.json(transactions);
    
  });
};

exports.read_account_specific_transactions = function(req, res) {
  logger.info("Controller: Read some accounts's specific transactions");
  logger.info("accId:"+req.params.accId);
  logger.info("Headers customerId:"+req.headers.customerid);
  Accounts.find({'customer_id':req.headers.customerid, 'acc_id':req.params.accId}, function(err, account) {
    if (err)
    {
      logger.debug("read_account_specific_transactions. Read user's specific accounts error: "+err);
      // res.send(err);
    }
    logger.debug("read_account_specific_transactions. Found accounts: "+JSON.stringify(account));
    logger.debug("accountSize"+account.length);
    if(account.length == 0)
      res.status(401).json({message: 'Authorization Error. User with a valid token trying to access another user transactions'});
    // res.json(account);
  });

  Transactions.find({'acc_id':req.params.accId}, function(err, transactions) {
    if (err)
      res.send(err);
    res.json(transactions);
  });
};
