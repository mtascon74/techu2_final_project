// HTTPS Back Server for Final Project Tech University
// transationsModel. Use Mongoose to map to transactions collection
// Author: Manuel Tascon Alvarez

'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var TransactionsSchema = new Schema({
  customer_id: {
    type: String
  },
  acc_id: {
    type: String
  },
  transaction_id: {
    type: String
  },
  transaction_date: {
    type: Date
  },
  transaction_description: {
    type: String
  },
  transaction_amount: {
    type: Number
  }   
});

module.exports = mongoose.model('Transactions', TransactionsSchema);
