// HTTPS Back Server for Final Project Tech University
// accountsModel. Use Mongoose to map to accounts collection
// Author: Manuel Tascon Alvarez

'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var AccountsSchema = new Schema({
  customer_id: {
    type: String,
    required: 'Kindly enter the id of the customer'
  },
  acc_id: {
    type: String
    // required: 'Kindly enter the id of the account'
  },
  acc_IBAN: {
    type: String
  },
  acc_alias: {
    type: String
  },
  acc_type: {
    type: String
  },
  acc_balance: {
    type: String
  } 
});

module.exports = mongoose.model('Accounts', AccountsSchema);